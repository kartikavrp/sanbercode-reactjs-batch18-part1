// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var gabung = kataPertama + " " + kataKedua[0].toUpperCase() + kataKedua.slice(1) + " " + kataKetiga + " " + kataKeempat.toUpperCase();
console.log("-------------------------------------------------------------------------\n");
console.log(gabung);


// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var total = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat)
console.log("-------------------------------------------------------------------------\n");
console.log(total);


// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25);

console.log("-------------------------------------------------------------------------\n");
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


// soal 4
var nilai = 80;
var indeks;

if (nilai >= 80 ) {
	indeks = "A";
} else if (nilai >= 70) {
	indeks = "B";
} else if (nilai >= 60) {
	indeks = "C";
} else if (nilai >= 50) {
	indeks = "D";
} else {
	indeks = "E";
}

console.log("-------------------------------------------------------------------------\n");
console.log("Nilai  : " + nilai);
console.log("Indeks : " + indeks);


// soal 5
var tanggal = 18;
var bulan = 7;
var tahun = 1994;
var strBulan;

switch(bulan) {
	case 1  : { strBulan = "Januari"; break; }
	case 2  : { strBulan = "Februari"; break; }
	case 3  : { strBulan = "Maret"; break; }
	case 4  : { strBulan = "April"; break; }
	case 5  : { strBulan = "Mei"; break; }
	case 6  : { strBulan = "Juni"; break; }
	case 7  : { strBulan = "Juli"; break; }
	case 8  : { strBulan = "Agustus"; break; }
	case 9  : { strBulan = "September"; break; }
	case 10 : { strBulan = "Oktober"; break; }
	case 11 : { strBulan = "November"; break; }
	case 12 : { strBulan = "Desember"; break; }
}

var strTglLahir = String(tanggal) + " " + strBulan + " " + String(tahun);
console.log("-------------------------------------------------------------------------\n");
console.log(strTglLahir);
console.log("-------------------------------------------------------------------------");

