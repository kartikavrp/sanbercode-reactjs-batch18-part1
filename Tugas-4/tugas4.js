// soal 1
console.log("-------------------------------------------------------------------------\n");

var i = 0;
console.log("LOOPING PERTAMA");
while (i < 20) {	
	i+=2;
	console.log(i + " - I love coding")		
}

console.log("LOOPING KEDUA");
while (i > 0) {	
	console.log(i + " - I will become a frontend developer")	
	i-=2;
}


// soal 2
console.log("-------------------------------------------------------------------------\n");

for (var x=1; x<=20; x++) {
	if (x%2 == 0) {
		console.log(x + " - Berkualitas");
	} else {
		if (x%3 == 0) {
			console.log(x + " - I love Coding");
		} else {
			console.log(x + " - Santai");
		}
	}
}


// soal 3
console.log("-------------------------------------------------------------------------\n");

for (var y=1; y<=7; y++) {
	x = y;
	result = "";
	while ( x>0 ) {
		result = result + "#";
		x-=1;
	}
	console.log(result);
}


// soal 4
console.log("-------------------------------------------------------------------------\n");

var kalimat="saya sangat senang belajar javascript";
var kata = kalimat.split(" ");
console.log(kata);


// soal 5
console.log("-------------------------------------------------------------------------\n");

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();

for (a=0; a<daftarBuah.length; a++) {
	console.log(daftarBuah[a]);
}

console.log("-------------------------------------------------------------------------");