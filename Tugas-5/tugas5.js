// soal 1
console.log("-------------------------------------------------------------------------\n");

function halo() {
  return "Halo Sanbers!";
}
console.log(halo());


// soal 2
console.log("-------------------------------------------------------------------------\n");

function kalikan(a,b) {
	return a*b;
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);


// soal 3
console.log("-------------------------------------------------------------------------\n");

function introduce(name, age, address, hobby) {
	return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobi yaitu " + hobby;
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan); 


// soal 4
console.log("-------------------------------------------------------------------------\n");

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var objDaftarPeserta   = {
	nama            : arrayDaftarPeserta[0], 
	"jenis kelamin" : arrayDaftarPeserta[1], 
	hobi            : arrayDaftarPeserta[2],
	"tahun lahir"   : arrayDaftarPeserta[3]
}

console.log(objDaftarPeserta);


// soal 5
console.log("-------------------------------------------------------------------------\n");

var buah = [{nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000}, 
		    {nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000}, 
		    {nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada", harga: 10000}, 
		    {nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000}];

console.log(buah[0]);


// soal 6
console.log("-------------------------------------------------------------------------\n");

var dataFilm = []
function addDataFilm(Nama, Durasi , Genre, Tahun) {
	var data = {
		nama   : Nama,
		durasi : Durasi,
		genre  : Genre,
		tahun  : Tahun
	}
	dataFilm.push(data);
}

addDataFilm("Train to Busan", "1h 58min", "Action, Horor, Thriller", "2016");
addDataFilm("Interstellar", "2h 49min", "Adventure, Drama, Sci-Fi ", "2014");
addDataFilm("Harry Potter and the Deathly Hallows: Part 2", "2h 10min ", "Adventure, Drama, Fantasy", "2011");
console.log(dataFilm);
console.log("-------------------------------------------------------------------------");

