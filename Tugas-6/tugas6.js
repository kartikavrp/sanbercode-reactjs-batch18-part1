//soal 1
console.log("-------------------------------------------------------------------------\n");

const luasLingkaran = (r) => {
	let phi = 22/7;
	let luas = phi*r*r;
    return `Luas lingkaran dengan r = ${r} adalah ${luas}`;
}

const kelilingLingkaran = (r) => {
	let phi = 22/7;
    let keliling = 2*phi*r;
    return `Keliling lingkaran dengan r = ${r} adalah ${keliling}`;
}

console.log(luasLingkaran(4));
console.log(kelilingLingkaran(4));


//soal 2
console.log("-------------------------------------------------------------------------\n");

let kalimat = ""
const tambahKata = (kata) => {
	kalimat += ` ${kata}`;
}
tambahKata("saya");
tambahKata("adalah");
tambahKata("seorang");
tambahKata("frontend");
tambahKata("developer");
console.log(kalimat);


//soal 3
console.log("-------------------------------------------------------------------------\n");

const newFunction = (firstName, lastName) => {
    return {
    firstName,
    lastName,
    fullName: function(){
    	console.log(firstName + " " + lastName);      
    }
  }
}

newFunction("William", "Imoh").fullName();


//soal 4
console.log("-------------------------------------------------------------------------\n");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation);


//soal 5
console.log("-------------------------------------------------------------------------\n");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
//Driver Code
console.log(combined)
