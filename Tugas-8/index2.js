var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function execute(time, book) {
	if (i < book.length) {
		readBooksPromise(time, book[i]).then (
			function(sisaWaktu) {
				if (time >= book[i].timeSpent) {				
					i++;				
					execute(sisaWaktu, books);			
				}		
			}			
		)
	}
}

var time = 10000;
var i = 0;
execute(time, books);